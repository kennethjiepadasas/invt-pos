import React from 'react'
import { Input, Row, Button, Col, PageHeader } from 'antd';

const SingleInputForm = ({
    placeholder = "Placeholder",
    buttonName = "Add",
    value = "",
    handleSubmit = () => { },
    handleCategoryInput,
}) => {
    return (
        <Row gutter={16}>
            <Col span={6} >
                <Input value={value} onChange={handleCategoryInput} placeholder={placeholder} />
            </Col>
            <Col span={18} >
                <Button onClick={handleSubmit}>{buttonName}</Button>
            </Col>
        </Row >
    )
}

export default SingleInputForm