import React from 'react'
import { List } from 'antd';


const DataList = ({data}) => {
    return (
        <List
            size="large"
            style={{margin:'24px 0px'}}
            header={<div>Header</div>}
            footer={<div>Footer</div>}
            bordered
            dataSource={data}
            renderItem={item => <List.Item>{item.name}</List.Item>}
        />
    )
}

export default DataList