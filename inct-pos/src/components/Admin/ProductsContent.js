import React from 'react'
import { PageHeader, Layout, Button } from 'antd'
import { SingleInputForm } from '../'
const { Header, Content, Footer, Sider } = Layout;

const ProductContent = () => {
    return (
        <Content style={{ padding: '24px 24px', minHeight: '100vh' }}>
            <PageHeader
                title="Products"
                subTitle="Manage your Products Here"
            />
            <Content style={{ padding: '0px 24px' }}>
                <Button>Add Product</Button>
            </Content>
        </Content>
    )
}

export default ProductContent