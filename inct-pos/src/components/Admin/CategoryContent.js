import React from 'react'
import { PageHeader, Layout } from 'antd'
import { inject, observer } from 'mobx-react'
import { SingleInputForm, DataList } from '../'
import { observe } from 'mobx';
import { OmitProps } from 'antd/lib/transfer/renderListBody';
const { Header, Content, Footer, Sider } = Layout;
const CategoryContents = ({ category }) => {

    const handleCategoryInput = (e) => {
        category.currentCategory.name = e.target.value
    }
    return (

        <Content style={{ padding: '24px 24px', minHeight: '100vh' }}>
            <PageHeader
                title="Category"
                subTitle="Manage your Categories Here"
            />
            <Content style={{ padding: '0px 24px' }}>
                <SingleInputForm
                    value={category.currentCategory.name}
                    placeholder="Category Name"
                    handleCategoryInput={handleCategoryInput}
                    buttonName="Add"
                    handleSubmit={category.addCategory} />
                <DataList data={category.categories} />
            </Content>
        </Content>
    )
}

export default (inject('category'))(observer(CategoryContents))