import NavStore from './NavStore'
import CategoryStore from './CategoryStore'
const navStore = new NavStore()
const category = new CategoryStore()
const stores = {
    navStore, category
}
export default stores