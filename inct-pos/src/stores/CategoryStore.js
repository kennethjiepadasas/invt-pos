import { decorate, observable, action } from "mobx";
import { Category } from '../models'
import { firestore } from '../config/firebase'
import { async } from "q";
import { createDeflateRaw } from "zlib";
class CategoryStore {
    currentCategory = new Category()
    categories = []
    collection = firestore.collection('category')

    constructor() {
        this.getCategories()
    }
    addCategory = () => {
        this.currentCategory._id = this.collection.doc().id
        console.log(this.currentCategory);
        let { _id, dateCreated, name } = this.currentCategory
        this.collection.doc(this.currentCategory._id).set({ _id, dateCreated, name }).then(() => (
            this.currentCategory = new Category()
        ))
    }

    getCategories = () => {
        let { pushCategory,categories } = this
        this.collection.onSnapshot(snapshot => {
            if (this.categories.length) {
                snapshot.docChanges().forEach(function (change) {
                    if (change.type === "added") {
                        console.log("New city: ", change.doc.data());
                        pushCategory(new Category(change.doc.data()))
                        console.log(typeof categories);
                    }
                    if (change.type === "modified") {
                        categories = snapshot.docs.map(doc => new Category(doc.data()))
                        console.log("Modified city: ", change.doc.data());
                    }
                    if (change.type === "removed") {
                        console.log("Removed city: ", change.doc.data());
                    }
                });
            } else {
                this.categories = snapshot.docs.map(doc => new Category(doc.data()))
            }
            console.log(this.categories);

        })
    }
    pushCategory = (data) => {
        this.categories.push(new Category(data))
    }
    
}

decorate(CategoryStore, {
    currentCategory: observable,
    addCategory: action,
    categories:observable,
    getCategories:action,
    pushCategory:action
});

export default CategoryStore;
