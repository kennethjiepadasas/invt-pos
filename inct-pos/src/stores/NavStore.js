import { decorate, observable, action } from "mobx";
import createBrowserHistory from "history/createBrowserHistory";

class NavStore {
  history = createBrowserHistory();

  push = route => this.history.push(route);

  goBack = () => this.history.goBack();

  goForward = () => this.history.goForward();
}

decorate(NavStore, {
  history: observable,
  push: action,
  goBack: action,
  goForward: action
});

export default NavStore;
