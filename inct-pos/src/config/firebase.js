import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAYVG8R1wDivLIq_WMYAs0_pxM6qg14KW4",
    authDomain: "invt-pos.firebaseapp.com",
    databaseURL: "https://invt-pos.firebaseio.com",
    projectId: "invt-pos",
    storageBucket: "invt-pos.appspot.com",
    messagingSenderId: "397045908243",
    appId: "1:397045908243:web:f5559f0cff83601a4a50e6",
    measurementId: "G-MCBRQDV5X5"
}

const app = !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();


const firestore = app.firestore()
const auth = app.auth()
export { firestore, auth, firebase }