import { decorate, observable } from "mobx";
import { firebase } from '../config/firebase'
import Model from './model'
class Category extends Model {
    constructor(props) {
        const defaults = {
            _id: '',
            name: '',
            dateCreated: firebase.firestore.FieldValue.serverTimestamp()
        }
        super({ ...defaults, ...props })
    }
}

decorate(Category, {
    _id: observable,
    name: observable,
    dateCreated: observable
})

export default Category
