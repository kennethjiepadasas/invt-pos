import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from "mobx-react";
import { Router } from "react-router";
import { Admin } from './page'
import stores from './stores'
function App() {
  return (
    <Provider {...stores}>
      <Router history={stores.navStore.history}>
        <Admin />
      </Router>
    </Provider>
  );
}

export default App;
