import React, { Component } from 'react';
import { Layout, Menu, Breadcrumb, Anchor, Icon, PageHeader } from 'antd';
import { CategoryContents, ProductContent } from '../components'

import { Route, Link } from 'react-router-dom'

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

class Admin extends Component {
    render() {
        return (
            <Layout>
                <Content style={{ height: '100%' }}>
                    <Layout style={{ padding: '24px 0', background: '#fff' }}>
                        <Sider width={200} style={{ background: '#fff' }}>
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['sub1']}
                                style={{ height: '100%' }}
                            >
                                <SubMenu
                                    key="sub1"
                                    title={
                                        <span>
                                            <Icon type="user" />
                                            Inventory
                                        </span>
                                    }
                                >
                                    <Menu.Item key="1"><Link to="/admin/">Products</Link></Menu.Item>
                                    <Menu.Item key="2"><Link to="/admin/category">Products</Link></Menu.Item>
                                    <Menu.Item key="3">Type</Menu.Item>
                                </SubMenu>
                                <SubMenu
                                    key="sub2"
                                    title={
                                        <span>
                                            <Icon type="laptop" />
                                            subnav 2
                                        </span>
                                    }
                                >
                                    <Menu.Item key="5">option5</Menu.Item>
                                    <Menu.Item key="6">option6</Menu.Item>
                                    <Menu.Item key="7">option7</Menu.Item>
                                    <Menu.Item key="8">option8</Menu.Item>
                                </SubMenu>
                                <SubMenu
                                    key="sub3"
                                    title={
                                        <span>
                                            <Icon type="notification" />
                                            subnav 3
                                        </span>
                                    }
                                >
                                    <Menu.Item key="9">option9</Menu.Item>
                                    <Menu.Item key="10">option10</Menu.Item>
                                    <Menu.Item key="11">option11</Menu.Item>
                                    <Menu.Item key="12">option12</Menu.Item>
                                </SubMenu>
                            </Menu>
                        </Sider>
                        <Route exact path="/admin/" component={ProductContent} />
                        <Route exact path="/admin/category" component={CategoryContents} />
                    </Layout>
                </Content>
                <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>)
    }
}

export default Admin


